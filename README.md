# heimdall-build

[Heimdall](https://www.glassechidna.com.au/heimdall/) x86-64 binary for Ubuntu 18.04 LTS and build instructions

From Heimdall website:
> Heimdall is a cross-platform open-source tool suite used to flash firmware (aka ROMs) onto Samsung mobile devices.

# Quick install
[Download](https://framagit.org/am97/heimdall-build/-/archive/master/heimdall-build-master.zip) this repo, extract it, and copy `heimdall` and `heimdall-frontend` to `/usr/local/bin`

# Note
On Ubuntu, for some operations like `heimdall flash` you need to use `sudo`

# How to build
_(This is a simplified version of https://forum.xda-developers.com/note-4/general/heimdall-ubuntu-14-04-samsung-n910f-t3290725)_

1. Install requiered tools and dependencies:
```sudo apt-get install git build-essential cmake zlib1g-dev qt5-default libusb-1.0-0-dev libgl1-mesa-glx libgl1-mesa-dev```
2. Clone Heimdall repo:
``` git clone https://gitlab.com/BenjaminDobell/Heimdall && cd ./Heimdall```
3. Create build directory:
``` mkdir build && cd ./build ```
4. Run cmake and make:
``` cmake -DCMAKE_BUILD_TYPE=Release .. && make ```
5. Executable files should be in `./bin`. You can copy them to `/usr/local/bin`
``` sudo cp bin/* /usr/local/bin ```
6. Test Heimdall:
``` heimdall version```
should return something like
``` v1.4.2 ```
